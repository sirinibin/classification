from sklearn import datasets
from sklearn import svm  # Classic algorithm in machine learning to classify
from sklearn import tree
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier

wine = datasets.load_wine()

features = wine.data
labels = wine.target
"""
print("Number of entries:" + str(len(features)))

for featurename in wine.feature_names:
    print(featurename[:10], end="|  "),

print("Class")

for feature, label in zip(features, labels):
    for f in feature:
        print(str(f), end="|  "),
    print(label)
"""
print("features:")
print(features)
print("Labels:")
print(labels)
train_features, test_features, train_labels, test_labels = train_test_split(
    features, labels, test_size=0.2)  # 20% for testing

"""
print("Number of entries:" + str(len(train_features)))

for featurename in wine.feature_names:
    print(featurename[:10], end="|  "),

print("Class")

for feature, label in zip(train_features, train_labels):
    for f in feature:
        print(str(f), end="|  "),
    print(label)
"""
#classifier = svm.SVC(kernel="linear")
#classifier = tree.DecisionTreeClassifier()
classifier = RandomForestClassifier()
# print(train_labels)
# print(test_labels)
# train
classifier.fit(train_features, train_labels)


# test/ predictions

predictions = classifier.predict(test_features)

print("Predictions:")
print(predictions)

score = 0
for i in range(len(predictions)):
    if predictions[i] == test_labels[i]:
        score += 1
accuracy = round((score/len(predictions)) * 100, 2)

print("Accuracy:"+str(accuracy)+"%")
