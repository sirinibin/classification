import csv
from datetime import datetime, timedelta
from dateutil.parser import parse
import plotly.graph_objects as go
import plotly.figure_factory as ff
import random
from pprint import pprint
from time import strftime
from time import gmtime
from sklearn import datasets
from sklearn import svm  # Classic algorithm in machine learning to classify
from sklearn import tree
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from operator import itemgetter


class Classifier:
    csvFileName = "Case_B.csv"

    classifier = RandomForestClassifier()
    #classifier = tree.DecisionTreeClassifier()
    #classifier = svm.SVC()

    # labels & features Obtained from csv
    labels = []  #tv status values
    features = [] #house no. , time, tv consumption & aggr. consumption

    # Labels & Features used for Machine Learning
    train_labels = [] #tv status values
    train_features = [] #house no. , time, tv consumption & aggr. consumption

    # Labels & Features used for Testing
    test_labels = [] #tv status values
    test_features = [] #house no. , time, tv consumption & aggr. consumption

    # % of Data we need to test from the given data
    testPercent = 10

    # Predictions/Test result(ie.,Tv Status) obtained after testing against the trained data
    predictions = []

    # Accuracy % of Prediction/Test result
    accuracy = 0.0

    tvOffLimit = 1  # 1KW
    tvStandByLimit = 15  # 15KW

    def __init__(self, csvFileName):
        self.csvFileName = csvFileName
        self.parseCSV()

    def parseCSV(self):
        with open(self.csvFileName, newline='', encoding='utf-8') as csvfile:
            readCSV = csv.reader(csvfile, delimiter=",")
            self.csvData = {}
            i = 0
            for row in readCSV:
                i += 1
                if len(row) == 4 and i >= 3:
                    try:
                        house = int(float(row[0]))
                        time = int(float(row[1]))
                        tvPowerConsumption = float(row[2])
                        aggregatePowerConsumption = float(row[3])

                        tv_status = ""
                        if tvPowerConsumption <= self.tvOffLimit:
                            tv_status = "OFF"
                        elif tvPowerConsumption > self.tvOffLimit and tvPowerConsumption <= self.tvStandByLimit:
                            tv_status = "STANDBY"
                        elif tvPowerConsumption > self.tvStandByLimit:
                            tv_status = "ON"

                        self.features.append(
                            [house, time, tvPowerConsumption, aggregatePowerConsumption])
                        self.labels.append(tv_status)

                    except ValueError:
                        continue

            return self.csvData

    def train(self):
        print("\nTotal no.of records:"+str(len(self.features)))
        # Splitting training data & testing data
        self.train_features, self.test_features, self.train_labels, self.test_labels = train_test_split(
            self.features, self.labels, test_size=(round(self.testPercent/100, 2)))  # 20% for testing

        # perform training
        self.classifier.fit(self.train_features, self.train_labels)

        print("\nTraining done with " +
              str(len(self.train_features))+"("+str(100-self.testPercent)+"%) random records")

    def test(self):
        # print("Total no.of testing records:"+str(len(self.test_features)))
        self.predictions = self.classifier.predict(self.test_features)
        print("\nTesting done with " +
              str(len(self.test_features))+"("+str(self.testPercent)+"%) random records")

        self.findAccuracy()

    def findAccuracy(self):
        score = 0
        for i in range(len(self.predictions)):
            if self.predictions[i] == self.test_labels[i]:
                score += 1
        self.accuracy = round((score/len(self.predictions)) * 100, 2)

        print("\nTesting/Predicting Accuracy:"+str(self.accuracy)+"%")
        return self.accuracy

    def getGraph3Data(self):
        data = {}
        train_features = sorted(self.train_features, key=itemgetter(1))
        for record in train_features:
            house = record[0]
            time = record[1]
            tvPowerConsumption = record[2]
            aggregatePowerConsumption = record[3]

            tvStatusIndex = self.train_features.index(record)
            tvStatus = self.train_labels[tvStatusIndex]

            if house not in data:
                data[house] = {}

            if "time" not in data[house]:
                data[house]["time"] = []

            if "tv_power_consumption" not in data[house]:
                data[house]["tv_power_consumption"] = []

            if "aggr_power_consumption" not in data[house]:
                data[house]["aggr_power_consumption"] = []

            data[house]["time"].append(time)

            data[house]["tv_power_consumption"].append(
                tvPowerConsumption)

            data[house]["aggr_power_consumption"].append(
                aggregatePowerConsumption)

            if "tv_text" not in data[house]:
                data[house]["tv_text"] = []

            if "aggr_text" not in data[house]:
                data[house]["aggr_text"] = []

            timeStr = strftime("%H:%M", gmtime(time*60))

            tv_text = "House:" + \
                str(house)+"<br>Time:"+timeStr + "<br>Status:"+tvStatus + \
                "<br>TV Power consumption:" + \
                str(tvPowerConsumption)+" KW"

            aggr_text = "House:" + \
                str(house)+"<br>Time:"+timeStr + \
                "<br>Aggregate Power consumption:" + \
                str(aggregatePowerConsumption)+" KW"

            data[house]["tv_text"].append(tv_text)
            data[house]["aggr_text"].append(aggr_text)

        return data

    def generateGraph3(self):
        fig = go.Figure({
            "layout": {
                "title": {"text": "Training done with " +
                          str(len(self.train_features))+"("+str(100-self.testPercent)+"%) random records"},
                "xaxis_title": {"text": "Time (mins)"},
                "yaxis_title": {"text": "Power Consumption (KW)"}
            },
        })

        data = self.getGraph3Data()
        for house in data:
            fig.add_trace(go.Scatter(x=data[house]["time"], y=data[house]["tv_power_consumption"], name="House "+str(house),
                                     text=data[house]["tv_text"],
                                     hoverinfo='text+name',
                                     line_shape='hv'))

        fig.update_traces(hoverinfo='text+name', mode='lines+markers')
        fig.update_layout(legend=dict(
            y=0.5, traceorder='reversed', font_size=16))

        fig.show()

    def getGraph4Data(self):
        data = {}
        test_features = sorted(self.test_features, key=itemgetter(1))
        for record in test_features:
            house = record[0]
            time = record[1]
            tvPowerConsumption = record[2]
            aggregatePowerConsumption = record[3]

            tvStatusIndex = self.test_features.index(record)
            tvStatus = self.test_labels[tvStatusIndex]

            # Get Predicted / Test result
            predictedTvStatus = self.predictions[tvStatusIndex]

            if house not in data:
                data[house] = {}

            if "time" not in data[house]:
                data[house]["time"] = []

            if "tv_power_consumption" not in data[house]:
                data[house]["tv_power_consumption"] = []

            if "aggr_power_consumption" not in data[house]:
                data[house]["aggr_power_consumption"] = []

            if "tv_status" not in data[house]:
                data[house]["tv_status"] = []

            if "predicted_tv_status" not in data[house]:
                data[house]["predicted_tv_status"] = []

            data[house]["time"].append(time)

            data[house]["tv_power_consumption"].append(
                tvPowerConsumption)

            data[house]["aggr_power_consumption"].append(
                aggregatePowerConsumption)

            data[house]["tv_status"].append(tvStatus)
            data[house]["predicted_tv_status"].append(predictedTvStatus)

            if "tv_text" not in data[house]:
                data[house]["tv_text"] = []

            if "aggr_text" not in data[house]:
                data[house]["aggr_text"] = []

            timeStr = strftime("%H:%M", gmtime(time*60))

            tv_text = "House:" + \
                str(house)+"<br>Time:"+timeStr + "<br>Original Status:"+tvStatus + \
                "<br>Predicted Status/Test Result:"+predictedTvStatus + \
                "<br>TV Power consumption:" + \
                str(tvPowerConsumption)+" KW"

            aggr_text = "House:" + \
                str(house)+"<br>Time:"+timeStr + \
                "<br>Aggregate Power consumption:" + \
                str(aggregatePowerConsumption)+" KW"

            data[house]["tv_text"].append(tv_text)
            data[house]["aggr_text"].append(aggr_text)

        return data

    def generateGraph4(self):
        fig = go.Figure({
            "layout": {
                "title": {"text": "Testing/Prediction done with ramaining " +
                          str(len(self.test_features))+"("+str(self.testPercent)+"%) random records, [Accuracy:"+str(self.accuracy)+"% , RED DOT=>PREDICTION FAILED,GREEN DOT=>PREDICTION SUCCEEDED]"},
                "xaxis_title": {"text": "Time (mins)"},
                "yaxis_title": {"text": "Power Consumption (KW)"}
            },
        })

        data = self.getGraph4Data()
        i = 0
        for house in data:
            fig.add_trace(go.Scatter(
                x=data[house]["time"],
                y=data[house]["tv_power_consumption"],
                name="House "+str(house),
                line=dict(
                            color=self.getRandomColorHex(),
                            ),
                marker=dict(
                            line=dict(
                                #color=lineColors[i], width=4
                                ),
                            size=7,
                            color=list(
                                map(
                                    self.SetColor,
                                    data[house]["tv_status"],
                                    data[house]["predicted_tv_status"],
                                ))),
                text=data[house]["tv_text"],
                hoverinfo='text+name',
                line_shape='hv'))
            i += 1

        fig.update_traces(hoverinfo='text+name', mode='lines+markers')
        fig.update_layout(legend=dict(
            y=0.5, traceorder='reversed', font_size=16))

        fig.show()

    def SetColor(self, status, predictedStatus):
        if status != predictedStatus:
            return "red"
        else:
            return "green"
    def getRandomColorHex(self):
        return "#{:06x}".format(random.randint(0, 0xFFFFFF))