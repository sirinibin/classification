import csv
from datetime import datetime, timedelta
from dateutil.parser import parse
import plotly.graph_objects as go
import plotly.figure_factory as ff
import random
from pprint import pprint
from time import strftime
from time import gmtime


class Tv:
    csvFileName = "Case_B.csv"
    csvData = {}
    houses = []
    tvOffLimit = 1  # 1KW
    tvStandByLimit = 15  # 15KW

    def __init__(self, csvFileName):
        self.csvFileName = csvFileName
        self.parseCSV()

    def parseCSV(self):
        with open(self.csvFileName, newline='', encoding='utf-8') as csvfile:
            readCSV = csv.reader(csvfile, delimiter=",")
            self.csvData = {}
            i = 0
            for row in readCSV:
                i += 1
                if len(row) == 4 and i >= 3:
                    try:
                        house = int(float(row[0]))
                        minute = int(float(row[1]))
                        tvPowerConsumption = float(row[2])
                        aggregatePowerConsumption = float(row[3])
                        if house not in self.houses:
                            self.houses.append(house)
                        """
                        self.csvData.append(
                            [house, minute, tvPowerConsumption, aggregatePowerConsumption])
                        """
                        if house not in self.csvData:
                            self.csvData[house] = {}

                        if "time" not in self.csvData[house]:
                            self.csvData[house]["time"] = []

                        if "tv_power_consumption" not in self.csvData[house]:
                            self.csvData[house]["tv_power_consumption"] = []

                        if "aggr_power_consumption" not in self.csvData[house]:
                            self.csvData[house]["aggr_power_consumption"] = []

                        self.csvData[house]["time"].append(minute)

                        self.csvData[house]["tv_power_consumption"].append(
                            tvPowerConsumption)

                        self.csvData[house]["aggr_power_consumption"].append(
                            aggregatePowerConsumption)

                        if "tv_text" not in self.csvData[house]:
                            self.csvData[house]["tv_text"] = []

                        if "aggr_text" not in self.csvData[house]:
                            self.csvData[house]["aggr_text"] = []

                        time = strftime("%H:%M", gmtime(minute*60))
                        tv_status = ""
                        if tvPowerConsumption <= self.tvOffLimit:
                            tv_status = "OFF"
                        elif tvPowerConsumption > self.tvOffLimit and tvPowerConsumption <= self.tvStandByLimit:
                            tv_status = "STANDBY"
                        elif tvPowerConsumption > self.tvStandByLimit:
                            tv_status = "ON"

                        tv_text = "House:" + \
                            str(house)+"<br>Time:"+time + "<br>Status:"+tv_status + \
                            "<br>TV Power consumption:" + \
                            str(tvPowerConsumption)+" KW"

                        aggr_text = "House:" + \
                            str(house)+"<br>Time:"+time + \
                            "<br>Aggregate Power consumption:" + \
                            str(aggregatePowerConsumption)+" KW"

                        self.csvData[house]["tv_text"].append(tv_text)
                        self.csvData[house]["aggr_text"].append(aggr_text)

                    except ValueError:
                        continue

            return self.csvData

    def generateGraph1(self):
        fig = go.Figure({
            "layout": {
                "title": {"text": "Tv Power Consumption"},
                "xaxis_title": {"text": "Time (mins)"},
                "yaxis_title": {"text": "Power Consumption (KW)"}
            },
        })

        for house in self.csvData:
            fig.add_trace(go.Scatter(x=self.csvData[house]["time"], y=self.csvData[house]["tv_power_consumption"], name="House "+str(house),
                                     text=self.csvData[house]["tv_text"],
                                     hoverinfo='text+name',
                                     line_shape='hv'))
        fig.update_traces(hoverinfo='text+name', mode='lines+markers')
        fig.update_layout(legend=dict(
            y=0.5, traceorder='reversed', font_size=16))

        fig.show()

    def generateGraph2(self):
        fig = go.Figure({
            "layout": {
                "title": {"text": "Aggr Power Consumption"},
                "xaxis_title": {"text": "Time (mins)"},
                "yaxis_title": {"text": "Power Consumption (KW)"},

            },
        })

        for house in self.csvData:
            fig.add_trace(go.Scatter(x=self.csvData[house]["time"], y=self.csvData[house]["aggr_power_consumption"], name="House "+str(house),
                                     text=self.csvData[house]["aggr_text"],
                                     hoverinfo='text+name',
                                     line_shape='spline'))

        fig.update_traces(hoverinfo='text+name', mode='lines+markers')
        fig.update_layout(legend=dict(
            y=0.5, traceorder='reversed', font_size=16))
        # fig.layout.yaxis2.update({'title': 'Power Consumption(KW)'})

        fig.show()

    def getRandomColorHex(self):
        return "#{:06x}".format(random.randint(0, 0xFFFFFF))
