import plotly.graph_objects as go
import plotly.figure_factory as ff
from classifier import Classifier

# Note: Install plotly by running pip install plotly==4.5.4 before running this

obj = Classifier("Case_B.csv")
obj.train()
obj.generateGraph3()
