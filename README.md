# How to run

## Clone the repo

`git clone https://gitlab.com/sirinibin/classification`

## Installdependancies

### Install pip3

```
$ sudo apt update
$ sudo apt install python3-pip
```
  
### Install plotly

`$ pip3 install plotly==4.10.0`

### Install Numby package
`$ sudo apt install python3-numpy`
### Install Scikit learn library
`$ pip3 install -U scikit-learn`


## Run the project

```
python3 graph1.py
```
>Tv power consumption 

![](https://i.imgur.com/bhpspWc.png)

```
python3 graph2.py
```

>Aggregate Power consumption

![](https://i.imgur.com/vCH63sS.png)

```
python3 graph3.py
```

> Training data plot
![](https://i.imgur.com/mQyyfo5.png)

```
python3 graph4.py
```

> Testing or prediction done 
![](https://i.imgur.com/hgnOsOe.png)

![](https://i.imgur.com/Qv9y3wE.png)
