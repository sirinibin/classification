def is_matched(S):
    stack = []
    if len(S) == 0:
        return True

    # Iterate over the string
    for character in S:
        if character == '[' or character == '{':
            stack.append(character)
            continue
        elif character == ']':
            try:
                lastItem = stack.pop()
            except (IndexError):
                return False

            if lastItem != '[':
                return False
        elif character == '}':
            try:
                lastItem = stack.pop()
            except (IndexError):
                return False

            if lastItem != '{':
                return False
        else:
            return False

    return len(stack) == 0


S = "{}[]{{[{}][]}}"

if is_matched(S):
    print("String \""+S+"\" is Matched")
else:
    print("String \""+S+"\" is Not Matched")
